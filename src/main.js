const express = require("express");
const makeStoppable = require("stoppable")
const http = require("http");
const bodyParser = require('body-parser');
const cookeiSession= require('cookie-session');
const db = require('./models/db')
const routes = require('./routes')
const authRoutes = require('./routes/auth')
const dotenv = require('dotenv');

dotenv.config({path:'../.env'});
const app = express();

app.use(
  cookeiSession({
    name:'session',
    keys:['sdfax123']
  })
)

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

db.connect(process.env.MONGODB_URL).then(console.log("connected")).catch(err=>console.log(err));

app.use(
  '/api/auth/',
  authRoutes())

app.use(
  '/api/tasks/',
  routes())


const server = makeStoppable(http.createServer(app));

module.exports = () => {
  const stopServer = () => {
    return new Promise((resolve) => {
      server.stop(resolve);
    })
  };

  return new Promise((resolve) => {
    server.listen(3000, () => {
      console.log('Express server is listening on http://localhost:3000');
      resolve(stopServer);
    });
  });
}
