const mongoose = require('mongoose');

const TaskModel =new mongoose.Schema({
    email: { 
        type: String, 
        ref: 'users',
        required:true,
    },
    title:{
        type: String,
        required:true,
        trim:true,
    },
    description:{
        type: String,
        required:true,
        trim:true, 
    },
    isDone:{
        type: Boolean,
        default :false,
        required:true,
    }
})

TaskModel.index({ email: 1, title: 1 }, { unique: true });

module.exports = mongoose.model('tasks',TaskModel)
