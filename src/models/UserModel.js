const mongoose = require('mongoose');
const emailValidator= require('email-validator');
const bcrypt = require('bcrypt');
const SALT_ROUNDS =12;

const UserSchema =new mongoose.Schema({
    email:{
        type  :String,
        required:true,
        index:{unique:true},
        trim:true,
        lowercase:true,
        validate:{
            validator:emailValidator.validate,
            message:props=>`${props.value} is not a valid email .`
        }
    },
    password:{
        type: String,
        required:true,
        trim:true,
        minLength:8
    }
})

UserSchema.pre('save',async function peSave(next){
    const user = this;
    if(!user.isModified('password')) return next();
    try{
       const hash = await bcrypt.hash(user.password, SALT_ROUNDS);
       user.password= hash;
       return next();
    }catch(err){
       return next(err);
    }
})

UserSchema.methods.comparePassowrds = async function comparePasswords(candidate){
    return bcrypt.compare(candidate, this.password);
}

module.exports = mongoose.model('users',UserSchema)