const mongoose = require('mongoose');

module.exports.connect= async connectionStr=> mongoose.connect(connectionStr, {useNewUrlParser:true, useUnifiedTopology:true});

