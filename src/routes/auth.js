const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const UserModel = require("../models/UserModel")

module.exports=()=>{

    router.post('/register',async (req,res)=>{
        try{
        const { email, password } = req.body;

        const user = new UserModel({
          email: email,
          password: password
        });

        const savedUser= await user.save();
        if(savedUser)
          return res.status(200).json({message:"A user was successfully registered."});

        }catch(err){
          if(err.code ==11000){
            return res.status(400).json({
              error: "InvalidRequest",
              message: "This Email is Already registered",
            });
          }
          if(err.errors.password.name=="ValidatorError"){
            return res.status(400).json({
              error: "InvalidRequest",
              message: err.errors.password.message,
            });
          }
            return res.status(500).json({
              type: "Server error",
              Error:err
          })
       }  
    })

    router.post('/login' ,async (req, res)=>{
        try{
            const { email, password } = req.body;

            const user = await UserModel.findOne({email:email}).exec()
              if(!user){
                  return res.status(400).json({
                  error: "InvalidRequest",
                  message: "invalid email or password",
                });
              }
            
              const passwordOk = await user.comparePassowrds(password);
              if(!passwordOk){
                 return res.status(400).json({
                  error: "InvalidRequest",
                  message: "invalid email or password",
                });
              }
            
              const token = jwt.sign({ email }, 'topSecret', { expiresIn: '1h' });
                return res.status(200).json({
                    user: { 
                        email 
                  }, 
                  token
                });
              }catch(err){
                return res.status(500).json({
                  type: "Server error",
                  Error:err
              })
        }
    })
    return router;
}