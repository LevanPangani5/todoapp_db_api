const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const TaskModel = require('../models/TaskModel');


async function isAuthenticated(req, res, next) {
  const authHeader = req.headers['authorization'];
  if (authHeader && authHeader.startsWith('Bearer ')) {
    const token = authHeader.split(' ')[1];  
    try {
      const decoded = jwt.verify(token, 'topSecret');
      req.user = decoded;
      next();
    } catch (err) {
      return res.status(401).json({
        error: 'Unauthorized',
        message: 'Invalid or expired token.',
      });
    }
  } else {
    return res.status(401).json({
      error: 'Unauthorized',
      message: 'Authorization token missing or invalid.',
    });
  }
}

module.exports=()=>{
 
    router.post('/',isAuthenticated ,async (req,res)=>{
      try{ 
      const {title,description}= req.body;
      if(!title || !description){
        return res.status(400).json({
          error:"Bad Request",
          message:"all fields are required "
        })
      }
      
       const result = await TaskModel.findOne({ email: req.user.email, title: title });

       if(result){
        return res.status(400).json({
          error:"Bad Request",
          message:"task with given title aleady exists."
        })
       } 

       const task = new TaskModel({
        email: req.user.email,
        title: title,
        description:description,
        isDone:false,
      });
     const saved = await  task.save();
     if(saved){
      return res.status(201).json({
        message:"task was added successfully "
      })
     }
      }catch(err){
        return res.status(500).json({
          type: "Server error",
          Error:err
      })
      }
    })

    router.get('/',isAuthenticated , async(req,res)=>{
      try{
        const tasks = await TaskModel.find({email:req.user.email});
        return res.status(200).json({tasks})

      }catch(err){
        return res.status(500).json({
          type: "Server error",
          Error:err
      })
      }
    })

    router.post('/done', isAuthenticated , async(req,res)=>{
      try{
      const{title}= req.body;
      if(!title){
        return res.status(400).json({
          error:"Bad Request",
          message:"title is required"
        })
      }
      const task = await TaskModel.findOne({email:req.user.email, title:title});
      if(!task){
        return res.status(400).json({
          error:"Bad Request",
          message:"task with given title does not exists."
        })
       } 
       task.isDone=true;
       const result = await task.save();
       if(result){
        return res.status(200).json({
          message:"task was marked as done ."
        })
       }
    }catch(err){
      return res.status(500).json({
        type: "Server error",
        Error:err
    })
    }
  })

    router.get('/done', isAuthenticated , async(req ,res)=>{
      try{
        const doneTasks = await TaskModel.find({email:req.user.email , isDone:true});
        return res.status(200).json({doneTasks})
      }catch(err){
        return res.status(500).json({
          type: "Server error",
          Error:err
      })
      }
    })

    router.put('/done/:taskTitle', isAuthenticated, async (req, res)=>{
      try{
      const currentTitle = req.params.taskTitle;
      const {title,description}= req.body;
      
      if(!title || !description||!currentTitle){
        return res.status(400).json({
          error:"Bad Request",
          message:"all three fields are required ."
        })
      }
      
      const newTitle = await TaskModel.findOne({email:req.user.email , title:title});
      if(newTitle){
        return res.status(400).json({
          error:"Bad Request",
          message:"task with given title aleady exists."
        })
      }
      const current = await TaskModel.findOne({email:req.user.email , title:currentTitle});
      if(!current){
        return res.status(400).json({
          error:"Bad Request",
          message:"task with given title does not exists."
        })
      }
      current.title = title;
      current.description=description;

      const ok = await current.save();

      if(ok){
        return res.status(200).json({message:"task updated successfully"})
      }

    }catch(err){
      return res.status(500).json({
        type: "Server error",
        Error:err
    })
    }
    })

    router.delete('/done', isAuthenticated, async(req, res)=>{
      try{
      const{title}= req.body;
      if(!title){
        return res.status(400).json({
          error:"Bad Request",
          message:"title is requied"
        })
      }

       await TaskModel.findOneAndDelete({email:req.user.email, title:title})
        return res.status(200).json({
          message:"A task was successfully deleted. Also, if a task with the provided title does not exist."
        })
  
      }catch(err){
        return res.status(500).json({
          type: "Server error",
          Error:err
      })
      }
    })
    return router;
}